package com.nivekaa.commonutils.common;

/**
 * The abstract Reference Prefix
 */
public interface ReferencePrefix {
  /**
   * The method which will overrided to retour the prefix
   */
  String getPrefix();
}
