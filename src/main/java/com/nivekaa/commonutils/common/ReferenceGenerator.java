package com.nivekaa.commonutils.common;

import com.github.f4b6a3.ksuid.Ksuid;
import com.github.f4b6a3.ksuid.KsuidCreator;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;

/** Le générateur de référence unique */
public final class ReferenceGenerator {

  private static final String SEPARATOR = "@";
  private static final String DEFAULT_PREFIX = "NOOP";

  /**
   * Le singleton de création d'une instance de la classe
   *
   * @return la classée suivant le modèle Singleton
   */
  public static ReferenceGenerator getInstance() {
    return new ReferenceGenerator();
  }

  /**
   * Création d'une ID unique plus un prefix
   *
   * @param prefix le préfixe
   * @return l'ID généré
   */
  public String next(@Nullable ReferencePrefix prefix) {
    String prefx = "";
    if (!Objects.isNull(prefix) && StringUtils.isNoneBlank(prefix.getPrefix())) {
      prefx = prefix.getPrefix() + SEPARATOR;
    } else {
      prefx = DEFAULT_PREFIX + SEPARATOR;
    }

    Ksuid ksuid = KsuidCreator.getSubsecondKsuid();
    return String.format("%s%s", prefx, ksuid);
  }

  /**
   * Création d'une ID unique sans un prefix
   *
   * @return l'ID généré
   */
  public String next() {
    return next(null);
  }
}
