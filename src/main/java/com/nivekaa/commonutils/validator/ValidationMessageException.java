package com.nivekaa.commonutils.validator;

import com.nivekaa.commonutils.exception.AbstractExceptionMessage;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * messages de l'exception {@link ValidationMessageException}
 *
 * @author kevin kemta (@niveka)
 */
@AllArgsConstructor
@NoArgsConstructor
public enum ValidationMessageException implements AbstractExceptionMessage {
  DEFAULT_MESSAGE("Errors count: %d global(s) and %d on the fields");

  private String description;

  /**
   * Recuperer la description du message d'erreur de l'exception
   *
   * @return le message a affiché a l'utilisateur
   */
  @Override
  public String getDescription() {
    return description;
  }
}
