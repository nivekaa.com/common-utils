package com.nivekaa.commonutils.validator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Created at : 04/12/2021 23:22 <br>
 * class: com.nivekaa.meelore.bootrunner.validator.ViolationErrorField <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public final class ViolationErrorField {
  private String message;
  private String fieldName;
  private Object rejectedValue;
}
