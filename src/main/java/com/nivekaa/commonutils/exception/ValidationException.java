package com.nivekaa.commonutils.exception;

import com.nivekaa.commonutils.validator.ViolationErrorField;
import java.util.Set;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created at : 04/12/2021 23:19 <br>
 * class: com.nivekaa.meelore.bootrunner.exception.ValidationException <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ValidationException extends AbstractException {
  @Getter private Set<ViolationErrorField> fields;

  public boolean hasViolationFields() {
    return fields != null && fields.size() != 0;
  }

  public ValidationException(AbstractExceptionMessage exceptionEnum) {
    super(HttpStatus.BAD_REQUEST, exceptionEnum.getDescription());
    fields = Set.of();
  }

  public ValidationException(AbstractExceptionMessage exceptionEnum, Object... args) {
    super(HttpStatus.BAD_REQUEST, String.format(exceptionEnum.getDescription(), args));
  }

  public ValidationException(String message, Set<ViolationErrorField> constraintViolations) {
    super(message);
    fields = constraintViolations;
  }

  public ValidationException(Set<ViolationErrorField> constraintViolations) {
    super("constraintViolations");
    fields = constraintViolations;
  }

  public ValidationException(
      Set<ViolationErrorField> constraintViolations,
      AbstractExceptionMessage exceptionEnum,
      Object... args) {
    super(String.format(exceptionEnum.getDescription(), args));
    fields = constraintViolations;
  }

  public ValidationException(
      AbstractExceptionMessage exceptionEnum, Set<ViolationErrorField> constraintViolations) {
    super(exceptionEnum.getDescription());
    fields = constraintViolations;
  }

  public ValidationException(String message) {
    super(message);
  }
}
