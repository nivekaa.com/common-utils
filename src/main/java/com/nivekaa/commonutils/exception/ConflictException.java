package com.nivekaa.commonutils.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created at : 04/12/2021 22:52 <br>
 * class: com.nivekaa.meelore.bootrunner.exception.ConflictException <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@ResponseStatus(value = HttpStatus.CONFLICT)
public class ConflictException extends AbstractException {
  public ConflictException() {
    super(HttpStatus.CONFLICT, "Conflict with resources!");
  }

  public ConflictException(AbstractExceptionMessage exceptionEnum) {
    super(HttpStatus.CONFLICT, exceptionEnum.getDescription());
  }

  public ConflictException(AbstractExceptionMessage exceptionEnum, Object... args) {
    super(HttpStatus.CONFLICT, String.format(exceptionEnum.getDescription(), args));
  }

  public ConflictException(String resourceValue) {
    super(HttpStatus.CONFLICT, String.format("There is conflict with resource %s!", resourceValue));
  }
}
