package com.nivekaa.commonutils.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created at : 04/12/2021 22:47 <br>
 * class: com.nivekaa.meelore.bootrunner.exception.BadRequestException <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends AbstractException {
  public BadRequestException(AbstractExceptionMessage message) {
    super(HttpStatus.BAD_REQUEST, message.getDescription());
  }

  public BadRequestException(AbstractExceptionMessage exceptionEnum, Object... args) {
    super(HttpStatus.BAD_REQUEST, String.format(exceptionEnum.getDescription(), args));
  }

  public BadRequestException(String message) {
    super(HttpStatus.BAD_REQUEST, message);
  }
}
