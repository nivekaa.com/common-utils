package com.nivekaa.commonutils.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created at : 04/12/2021 22:56 <br>
 * class: com.nivekaa.meelore.bootrunner.exception.UnauthorizedException <br>
 *
 * <p>Exception pour les Access denied
 *
 * @author kevin kemta (@niveka)
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class AccessDeniedException extends AbstractException {
  public AccessDeniedException() {
    super(HttpStatus.FORBIDDEN, "You are trying to access protected resources");
  }

  public AccessDeniedException(AbstractExceptionMessage exceptionEnum) {
    super(HttpStatus.FORBIDDEN, exceptionEnum.getDescription());
  }

  public AccessDeniedException(AbstractExceptionMessage exceptionEnum, Object... args) {
    super(HttpStatus.FORBIDDEN, String.format(exceptionEnum.getDescription(), args));
  }
}
