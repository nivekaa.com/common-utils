package com.nivekaa.commonutils.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created at : 04/12/2021 22:54 <br>
 * class: com.nivekaa.meelore.bootrunner.exception.ResourceNotFoundException <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends AbstractException {
  public ResourceNotFoundException() {
    super(HttpStatus.NOT_FOUND, "Could not find resource!");
  }

  public ResourceNotFoundException(AbstractExceptionMessage exceptionEnum) {
    super(HttpStatus.NOT_FOUND, exceptionEnum.getDescription());
  }

  public ResourceNotFoundException(AbstractExceptionMessage exceptionEnum, Object... args) {
    super(HttpStatus.NOT_FOUND, String.format(exceptionEnum.getDescription(), args));
  }

  public ResourceNotFoundException(String resourceValue) {
    super(HttpStatus.NOT_FOUND, String.format("Could not find resource %s!", resourceValue));
  }
}
