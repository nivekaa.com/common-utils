package com.nivekaa.commonutils.exception;

/**
 * Created at : 04/12/2021 22:42 <br>
 * class: com.nivekaa.meelore.bootrunner.exception.AbstractExceptionMessage <br>
 *
 * <p>Interface dont toutes les messages des exceptions heriterons
 *
 * @author kevin kemta (@niveka)
 */
public interface AbstractExceptionMessage {
  /**
   * La description de l'erreur (ici message).
   *
   * <p>La description peut etre une expression regulière
   *
   * @return le message d'erreur a affiché quand l'exeception sera lévée
   */
  String getDescription();
}
