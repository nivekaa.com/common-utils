package com.nivekaa.commonutils.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created at : 04/12/2021 22:54 <br>
 * class: com.nivekaa.meelore.bootrunner.exception.InternalErrorException <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalErrorException extends AbstractException {
  public InternalErrorException() {
    super(
        HttpStatus.INTERNAL_SERVER_ERROR,
        "It seems that the system is under maintenance. Please back soon or contact administrator to describe your disturbing");
  }

  public InternalErrorException(AbstractExceptionMessage exceptionEnum) {
    super(HttpStatus.INTERNAL_SERVER_ERROR, exceptionEnum.getDescription());
  }

  public InternalErrorException(AbstractExceptionMessage exceptionEnum, Object... args) {
    super(HttpStatus.INTERNAL_SERVER_ERROR, String.format(exceptionEnum.getDescription(), args));
  }

  public InternalErrorException(String message) {
    super(HttpStatus.INTERNAL_SERVER_ERROR, message);
  }
}
