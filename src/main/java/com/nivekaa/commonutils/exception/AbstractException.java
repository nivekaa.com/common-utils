package com.nivekaa.commonutils.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created at : 04/12/2021 22:44 <br>
 * class: com.nivekaa.meelore.bootrunner.exception.AbstractMLOException <br>
 *
 * <p>Classe abstraite qui sera heritée par toutes les exception du projet Meelore
 *
 * @author kevin kemta (@niveka)
 */
@Getter
@Setter
@ResponseStatus(HttpStatus.BAD_REQUEST)
public abstract class AbstractException extends RuntimeException {
  private HttpStatus status;
  private String message;

  protected AbstractException(String message) {
    super(message);
    this.message = message;
    status = HttpStatus.BAD_REQUEST;
  }

  protected AbstractException(HttpStatus status, String message) {
    super(message);
    this.message = message;
    this.status = status;
  }
}
