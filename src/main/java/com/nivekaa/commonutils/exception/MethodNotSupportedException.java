package com.nivekaa.commonutils.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created at : 04/12/2021 22:56 <br>
 * class: com.nivekaa.meelore.bootrunner.exception.UnauthorizedException <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class MethodNotSupportedException extends AbstractException {
  public MethodNotSupportedException() {
    super(
        HttpStatus.INTERNAL_SERVER_ERROR,
        "Something wrong. Same like the feature is yet under implementation or under maintenance. Please back soon or contact administrator ^^");
  }

  public MethodNotSupportedException(AbstractExceptionMessage exceptionEnum) {
    super(HttpStatus.INTERNAL_SERVER_ERROR, exceptionEnum.getDescription());
  }

  public MethodNotSupportedException(AbstractExceptionMessage exceptionEnum, Object... args) {
    super(HttpStatus.INTERNAL_SERVER_ERROR, String.format(exceptionEnum.getDescription(), args));
  }
}
