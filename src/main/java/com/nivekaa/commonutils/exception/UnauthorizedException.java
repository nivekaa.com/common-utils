package com.nivekaa.commonutils.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created at : 04/12/2021 22:56 <br>
 * class: com.nivekaa.meelore.bootrunner.exception.UnauthorizedException <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends AbstractException {
  public UnauthorizedException() {
    super(HttpStatus.UNAUTHORIZED, "Full authentication is required to get this resource.");
  }

  public UnauthorizedException(AbstractExceptionMessage exceptionEnum) {
    super(HttpStatus.UNAUTHORIZED, exceptionEnum.getDescription());
  }

  public UnauthorizedException(AbstractExceptionMessage exceptionEnum, Object... args) {
    super(HttpStatus.UNAUTHORIZED, String.format(exceptionEnum.getDescription(), args));
  }
}
