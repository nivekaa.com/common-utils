package com.nivekaa.commonutils.error;

import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * class utilitaire pour les erreurs
 *
 * @author kevin kemta (@niveka)
 */
@UtilityClass
public class ErrorUtil {
  /**
   * Convert a list of objects to the litteral string separated by ";"
   * @param values the list of objet to convert
   * @return the litteral string converted
   */
  public static String formatCdrLog(Object... values) {
    List<Object> list = Arrays.asList(values);
    list.replaceAll(el -> el != null ? el.toString().replaceAll("\n", "") : "");
    String format = String.join("", Collections.nCopies(list.size(), "%s;"));
    return String.format(format, values);
  }
}
