package com.nivekaa.commonutils.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * Created at : 04/12/2021 23:03 <br>
 * class: com.nivekaa.meelore.bootrunner.error.ApiValidationError <br>
 *
 * <p>Sous erreurs des exceptions de type Validation
 *
 * @author kevin kemta (@niveka)
 */
@Setter
@Getter
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class ApiValidationError extends ApiSubError {
  private String object;
  private String field;
  private Object rejectedValue;
  private String message;

  public ApiValidationError(String object, String message) {
    this.object = object;
    this.message = message;
  }
}
