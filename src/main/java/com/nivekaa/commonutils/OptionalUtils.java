package com.nivekaa.commonutils;

import java.util.Optional;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.ObjectUtils;

@UtilityClass
public class OptionalUtils {
  public <C> Optional<C> emptyIfNull(Optional<C> nullable) {
    return ObjectUtils.isEmpty(nullable) ? Optional.empty() : nullable;
  }

  public <C> C getOrNull(Optional<C> option) {
    return emptyIfNull(option).orElse(null);
  }
}
