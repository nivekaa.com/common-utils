package com.nivekaa.commonutils;

import java.lang.annotation.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * // "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.CONSTRUCTOR, ElementType.TYPE})
@Configuration
@Import(CommonModuleConfiguration.class)
@Documented
public @interface EnabledCommonModule {}
