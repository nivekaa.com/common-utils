package com.nivekaa.commonutils;

import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author kevin kemta (@niveka)
 */
@Configuration
@ComponentScan(
    basePackages = {
      "com.nivekaa.commonutils.common",
      "com.nivekaa.commonutils.entity",
      "com.nivekaa.commonutils.error",
      "com.nivekaa.commonutils.exception",
      "com.nivekaa.commonutils.model",
      "com.nivekaa.commonutils.validator",
    })
@Slf4j
public class CommonModuleConfiguration {
  /** Post method that will execute before all */
  @PostConstruct
  public void init() {
    log.info("The Meelore Boot Runner Module is running...");
  }
}
