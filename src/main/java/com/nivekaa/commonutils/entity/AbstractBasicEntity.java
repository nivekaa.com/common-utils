package com.nivekaa.commonutils.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Created at: 04/12/2021 22:31 <br>
 * class: com.nivekaa.meelore.bootrunner.entity.AbstractAuditingEntity <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@Setter
@Getter
@MappedSuperclass
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@SuperBuilder
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public abstract class AbstractBasicEntity extends AbstractEntity {
  @Version private int version;
}
