package com.nivekaa.commonutils.entity;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

/**
 * Created at : 04/12/2021 21:55 <br>
 * class: com.nivekaa.meelore.bootrunner.entity.AuditorAwareImpl <br>
 *
 * <p>l'Auditor Aware, classe permettant de recuperer l'ID de l'utilisateur connecté qui sera
 * ensuite utilisé par l'audit JPA
 *
 * @author kevin kemta (@niveka)
 */
@RequiredArgsConstructor
@Component
public class AuditorAwareImpl implements AuditorAware<String> {
  /**
   * Récupérer l'ID de l'utilisateur connecté.
   *
   * @return l'ID retrouvé (peut etre null dans de ce cas {@link Optional#empty()})
   */
  @Override
  public Optional<String> getCurrentAuditor() {
    return Optional.of("USR@DEFAULT");
  }
}
