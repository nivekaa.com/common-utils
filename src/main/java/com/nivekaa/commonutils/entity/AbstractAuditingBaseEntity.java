package com.nivekaa.commonutils.entity;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Version;
import java.time.OffsetDateTime;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Created at: 04/12/2021 22:24 <br>
 * class: com.nivekaa.meelore.bootrunner.entity.AbstractAuditingBaseEntity <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@Setter
@Getter
@MappedSuperclass
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
@SuperBuilder
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public abstract class AbstractAuditingBaseEntity extends AbstractEntity {

  @CreatedBy
  @Column(name = "created_by")
  private String createdBy;

  @CreatedDate
  @Column(name = "created_at", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
  private OffsetDateTime createdAt;

  @LastModifiedBy
  @Column(name = "last_modified_by")
  private String lastModifiedBy;

  @LastModifiedDate
  @Column(name = "updated_at", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
  private OffsetDateTime updatedAt;

  @Column(name = "deleted_at")
  private OffsetDateTime deletedAt;

  @Version private int version;
}
