package com.nivekaa.commonutils.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import lombok.SneakyThrows;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

/**
 * Created at : 07/12/2021 23:53 <br>
 * class: com.nivekaa.meelore.bootrunner.model.ApiResponse <br>
 *
 * <p>Le ResponseEntity basique construit pour généraliser les reponses client.
 *
 * @author kevin kemta (@niveka)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public record ApiResponse<T>(
    @JsonProperty(value = "data", required = true) T data,
    @JsonProperty("slide") Pagination pagination,
    @JsonProperty("success") Boolean success) {

  /**
   * Pour les operation qui sont passées avec succès
   *
   * @param dto la reponse de type {@link T} à retourner au client
   * @param <T> le type de retour la réponse
   * @return la reponse
   */
  public static <T> ApiResponse<T> ok(T dto) {
    return new ApiResponse<T>(dto, Pagination.nulle(), Boolean.TRUE);
  }

  /**
   * Pour les operations qui ont echoué et qu'on ne souhaite pas léver une exception
   *
   * @param dto la reponse qui sera nulle
   * @param <T> le type de la reponse (pas besoin dans le cas present)
   * @return la reponse
   */
  public static <T> ApiResponse<T> fail(T dto) {
    return new ApiResponse<>(dto, Pagination.nulle(), false);
  }

  /**
   * Pour les operation qui sont passées avec succès
   *
   * @param dtos la liste de reponses de type {@link T} à retourner au client
   * @param <T> le type de retour la réponse
   * @return la réponse
   */
  @SafeVarargs
  public static <T> ApiResponse<List<T>> of(T... dtos) {
    return new ApiResponse<>(List.of(dtos), Pagination.empty(), true);
  }

  /**
   * Pour les operation qui sont passées avec succès
   *
   * @return la réponse vide
   */
  public static <T> ApiResponse<T> empty() {
    return new ApiResponse<T>(null, Pagination.nulle(), Boolean.TRUE);
  }

  /**
   * Pour les operation qui sont passées avec succès + pagination
   *
   * @param page la Page de reponses de type {@link T} à retourner au client
   * @param uri l'Uri qui permettra de former les liens next et previous
   * @param query le filtre optimisé
   * @param sortBy l'élément d'ordre
   * @param <T> le type de retour la réponse
   * @return la réponse
   */
  public static <T> ApiResponse<List<T>> of(Page<T> page, String uri, String query, String sortBy) {
    if (!page.hasContent()) {
      return ApiResponse.empty();
    }
    int number = page.getNumber();
    Integer prev = page.hasPrevious() ? number - 1 : null;
    Integer next = page.hasNext() ? number + 1 : null;
    Pagination pagination =
        new Pagination()
            .setCurrentPage(number)
            .setNextPage(next)
            .setPreviousPage(prev)
            .setUri(uri)
            .setTotalPages(page.getTotalPages())
            .setPageSize(page.getNumberOfElements())
            .setTotalItems(page.getTotalElements());
    if (StringUtils.isNotEmpty(uri)) {
      query = StringUtils.defaultString(query);
      sortBy = StringUtils.defaultString(sortBy, getSortFromPage(page));
      uri =
          uri.concat(uri.contains("?") ? "&" : "?")
              .concat(
                  StringUtils.defaultString(
                      Pagination.QUERY_QUERY_PARAM + "=" + encodeUrl(query), ""))
              .concat(
                  StringUtils.defaultString("&" + Pagination.SORT_QUERY_PARAM + "=" + sortBy, ""));
      String nextUrl =
          page.hasNext() ? String.format("%s&page=%d&size=%d", uri, next, page.getSize()) : null;
      String prevUrl =
          page.hasPrevious()
              ? String.format("%s&page=%d&size=%d", uri, prev, page.getSize())
              : null;
      pagination.setNextUrl(nextUrl).setPreviousUrl(prevUrl);
    }
    return new ApiResponse<>(page.getContent().stream().toList(), pagination, Boolean.TRUE);
  }

  private static <T> String getSortFromPage(Page<T> page) {
    Sort sort =
        ObjectUtils.isNotEmpty(page.getSort())
            ? page.getSort()
            : (ObjectUtils.isNotEmpty(page.getPageable())
                ? page.getPageable().getSort()
                : Sort.unsorted());
    if (sort.isEmpty() || sort.isUnsorted()) {
      return "";
    }
    List<String> list = new ArrayList<>();
    for (Sort.Order order : sort) {
      list.add(order.getProperty() + "," + order.getDirection());
    }

    return StringUtils.join(list, "&");
  }

  /**
   * Pour les operation qui sont passées avec succès + pagination
   *
   * @param page la Page de reponses de type {@link T} à retourner au client
   * @param uri l'uri qui permettra de former les liens next et previous
   * @param <T> le type de retour la réponse
   * @return la reponse
   */
  public static <T> ApiResponse<List<T>> of(Page<T> page, String uri) {
    return of(page, uri, null, null);
  }

  /**
   * Pour les operation qui sont passées avec succès + pagination
   *
   * @param page la Page de reponses de type {@link T} à retourner au client
   * @param uri l'uri qui permettra de former les liens next et previous
   * @param queryParam the query parameter
   * @param <T> le type de retour la réponse
   * @return la reponse
   */
  public static <T> ApiResponse<List<T>> of(Page<T> page, String uri, String queryParam) {
    return of(page, uri, queryParam, null);
  }

  /**
   * Pour les operation qui sont passées avec succès + pagination
   *
   * @param page la Page de reponses de type {@link T} à retourner au client
   * @param <T> le type de retour la réponse
   * @return la reponse
   */
  public static <T> ApiResponse<List<T>> of(Page<T> page) {
    return of(page, null);
  }

  @SneakyThrows
  private static String encodeUrl(String value) {
    return URLEncoder.encode(value, StandardCharsets.UTF_8);
  }
}
