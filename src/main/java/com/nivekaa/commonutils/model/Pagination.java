package com.nivekaa.commonutils.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Created at : 11/12/2021 00:12 <br>
 * class: com.nivekaa.meelore.bootrunner.model.Pagination <br>
 *
 * <p>La classe de pagination pour les reponse de type liste
 *
 * @author kevin kemta (@niveka)
 */
@Setter
@Getter
@Accessors(chain = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public final class Pagination implements Serializable {
  private static final int DEFAULT_PAGE_SIZE = 25;
  public static final String SORT_QUERY_PARAM = "sort";
  public static final String QUERY_QUERY_PARAM = "query";
  private Integer currentPage;
  private long totalItems;
  private Integer pageSize;
  private Integer nextPage;
  private Integer previousPage;
  private int totalPages;

  // URL
  @JsonIgnore private String uri;
  private String nextUrl;
  private String previousUrl;

  /**
   * Instancier une Pagination vide (avecs le valeur par defaut)
   *
   * @return la pagination
   */
  public static Pagination empty() {
    return new Pagination()
        .setCurrentPage(0)
        .setNextPage(0)
        .setPreviousPage(0)
        .setPageSize(DEFAULT_PAGE_SIZE)
        .setTotalPages(0);
  }

  /**
   * Instancier une Pagination nulle (pour les reponses de type non-list)
   *
   * @return la pagination
   */
  public static Pagination nulle() {
    return null;
  }

  /**
   * Instancier une nouvelle pagination avec les valeurs suivantes:
   *
   * @param total noombre total d'elements en base (ou filtrer)
   * @param current le numéro current de la page
   * @param prev le numéro de la page suivante
   * @param next le numéro de la page précédente
   * @return la pagination instanciée
   */
  public static Pagination of(int total, int current, int prev, int next) {
    return new Pagination()
        .setCurrentPage(current)
        .setNextPage(next)
        .setPreviousPage(prev)
        .setPageSize(DEFAULT_PAGE_SIZE)
        .setTotalPages(total);
  }
}
