CREATE USER IF NOT EXISTS meelore_user@localhost IDENTIFIED BY 'meelore_pass';
SET PASSWORD FOR meelore_user@localhost = PASSWORD ('meelore_pass');
GRANT ALL ON *.* TO meelore_user@localhost WITH GRANT OPTION;
CREATE USER IF NOT EXISTS meelore_user@'%' IDENTIFIED BY 'meelore_pass';
SET PASSWORD FOR meelore_user@'%' = PASSWORD ('meelore_pass');
GRANT ALL ON *.* TO meelore_user@'%' WITH GRANT OPTION;

CREATE DATABASE IF NOT EXISTS meelore_db;
GRANT ALL ON meelore_db.* TO meelore_user@'%';

CREATE DATABASE IF NOT EXISTS fm_db;
GRANT ALL ON fm_db.* TO meelore_user@'%';

CREATE DATABASE IF NOT EXISTS id_db;
GRANT ALL ON id_db.* TO meelore_user@'%';
