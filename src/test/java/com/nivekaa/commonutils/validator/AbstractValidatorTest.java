package com.nivekaa.commonutils.validator;

import com.nivekaa.commonutils.exception.ValidationException;
import com.nivekaa.commonutils.fake.UserEntity;
import com.nivekaa.commonutils.fake.validator.TestValidator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AbstractValidatorTest {
  private final AbstractValidator<UserEntity> validator = new TestValidator(new UserEntity());

  @Test
  void isValid_KO() {
    assertFalse(validator.isValid());
  }

  @Test
  void isValid_OK() {
    assertTrue(new TestValidator(new UserEntity().setUsername("uname").setEmail("dddd")).isValid());
  }

  @Test
  void isValidOrThrow_KO() {
    Assertions.assertThatThrownBy(validator::isValidOrThrow)
        .isInstanceOf(ValidationException.class)
        .hasMessage("Errors count: 0 global(s) and 2 on the fields");
    assertThat(validator.getResult()).isNotNull();
    assertThat(validator.getResult().getAllErrors()).hasSize(2);
  }

  @Test
  void isValidOrThrow_OK() {
    assertTrue(
        new TestValidator(new UserEntity().setUsername("uname").setEmail("dddd")).isValidOrThrow());
    assertThat(validator.getResult()).isNotNull();
    assertThat(validator.getResult().getAllErrors()).isEmpty();
  }
}
