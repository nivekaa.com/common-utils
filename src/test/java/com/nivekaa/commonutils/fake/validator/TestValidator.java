package com.nivekaa.commonutils.fake.validator;

import com.nivekaa.commonutils.fake.UserEntity;
import com.nivekaa.commonutils.validator.AbstractValidator;
import org.apache.commons.lang3.StringUtils;

/**
 * Created at : 04/12/2021 23:45 <br>
 * class: com.nivekaa.meelore.bootrunner.fake.validator.TestValidator <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
public class TestValidator extends AbstractValidator<UserEntity> {
  public TestValidator(UserEntity target) {
    super(target, "USER_ENTITY");
  }

  @Override
  protected void validate() {
    testUsername();
    testEmail();
  }

  private void testEmail() {
    if (StringUtils.isBlank(getTarget().getEmail())) {
      getResult().rejectValue("email", "Email must not be blank");
    }
  }

  private void testUsername() {
    if (StringUtils.isEmpty(getTarget().getEmail())) {
      getResult().rejectValue("username", "Username must not be null or empty");
    }
  }
}
