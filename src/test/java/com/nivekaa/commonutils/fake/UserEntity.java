package com.nivekaa.commonutils.fake;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * Created at : 04/12/2021 23:46 <br>
 * class: com.nivekaa.meelore.bootrunner.fake.UserEntity <br>
 *
 * <p>// "add your class description here(inside de p tag)"
 *
 * @author kevin kemta (@niveka)
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class UserEntity {
  private String username;
  private String password;
  private String firstName;
  private String lastName;
  private String email;
}
