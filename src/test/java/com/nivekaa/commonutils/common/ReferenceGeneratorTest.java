package com.nivekaa.commonutils.common;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ReferenceGeneratorTest {

  @Test
  void next_noop() {
    assertThat(ReferenceGenerator.getInstance().next(null)).isNotNull().startsWith("NOOP@");
    assertThat(ReferenceGenerator.getInstance().next()).isNotNull().startsWith("NOOP@");
  }

  @Test
  void next_arobase() {
    Assertions.assertNotNull(ReferenceGenerator.getInstance().next(Prefix.INVESTOR));
    String refUser = ReferenceGenerator.getInstance().next(Prefix.USER);

    Assertions.assertNotNull(refUser);
    Assertions.assertTrue(refUser.startsWith(Prefix.USER.getPrefix() + "@"));
  }

  @Test
  void testNext() {
    Assertions.assertNotNull(ReferenceGenerator.getInstance().next());
  }

  enum Prefix implements ReferencePrefix {
    USER("USR"),
    INVESTOR(null);
    private final String prefix;

    Prefix(String s) {
      this.prefix = s;
    }

    @Override
    public String getPrefix() {
      return prefix;
    }
  }
}
