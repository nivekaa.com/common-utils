#!/usr/bin/env bash
ci_merge_targer_ref="${CI_COMMIT_TITLE}"
mvn_patching="mvn build-helper:parse-version versions:set \
  -DnewVersion=\${parsedVersion.majorVersion}.\${parsedVersion.minorVersion}.\${parsedVersion.nextIncrementalVersion}-SNAPSHOT \
   versions:commit "
mvn_hotfixing_or_releasing="mvn build-helper:parse-version versions:set \
  -DnewVersion=\${parsedVersion.majorVersion}.\${parsedVersion.nextMinorVersion}.0-SNAPSHOT \
  versions:commit "
mvn_majoring="mvn build-helper:parse-version versions:set \
  -DnewVersion=\${parsedVersion.nextMajorVersion}.\${parsedVersion.minorVersion}.0-SNAPSHOT \
  versions:commit "
source_branch=$(echo "$ci_merge_targer_ref" | grep -oP "branch '\K[^']*")

echo "" > /tmp/log.deploy

if [ "$source_branch" == "" ]; then
  echo "Error: Source branch not found" > /tmp/log.deploy
  echo ""
else
  if [[ "$source_branch" =~ ^(feature|bugfix|tech|enabler)/.+ ]]; then
    echo "It is the patch pipeline..." > /tmp/log.deploy
    echo "$mvn_patching"
  elif [[ "$source_branch" =~ ^release/.+ ]]; then
      echo "It is the major pipeline" > /tmp/log.deploy
      echo "$mvn_majoring"
  elif [[ "$source_branch" =~ ^(release|hotfix)/.+ ]]; then
    echo "It is the hotfix or release pipeline" > /tmp/log.deploy
    echo "$mvn_hotfixing_or_releasing"
  elif [[ "$source_branch" =~ ^ci-bot/.+ ]]; then
      echo "This branch is not configured to run pipeline" > /tmp/log.deploy
      echo ""
  else
    echo "The branch ${source_branch} can't match any pipeline configuration" > /tmp/log.deploy
    echo ""
  fi
fi